/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013-2019 Regents of the University of California.
 *
 * This file is part of ndn-cxx library (NDN C++ library with eXperimental eXtensions).
 *
 * ndn-cxx library is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * ndn-cxx library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and GNU Lesser
 * General Public License along with ndn-cxx, e.g., in COPYING.md file.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * See AUTHORS.md for complete list of ndn-cxx authors and contributors.
 *
 * @author Alexander Afanasyev <http://lasr.cs.ucla.edu/afanasyev/index.html>
 */

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include <ndn-cxx/security/signing-helpers.hpp>
#include <ndn-cxx/lp/tags.hpp>
#include <iostream>

// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
namespace ndn {
// Additional nested namespaces should be used to prevent/limit name conflicts
namespace examples {

class Producer
{
public:
  void
  run()
  {
    m_face.setInterestFilter("/example/testApp",
                             bind(&Producer::onInterest, this, _1, _2),
                             nullptr, // RegisterPrefixSuccessCallback is optional
                             bind(&Producer::onRegisterFailed, this, _1, _2));
    m_face.processEvents();
  } 

private:
  void
  onInterest(const InterestFilter&, const Interest& interest)
  {
    std::cout << ">> I: " << interest << std::endl;
    auto retranTimesTag = interest.getTag<lp::RetranTimesTag>()->get();
    std::cout<<"retranTimesTag is "<<retranTimesTag<< std::endl;

    auto fullRtt = interest.getTag<lp::FullRttTag>();
    if(fullRtt == nullptr){
      std::cout<<"not have this tag!\n";
    }else{
      std::cout<<"fullRtt->get() is "<<fullRtt->get()<<std::endl;
    }

    auto lastRtt = interest.getTag<lp::LastRttTag>();
    if(lastRtt == nullptr){
      std::cout<<"not have lastRttTag\n";
    }else{
      std::cout<<"lastRtt-<get() is "<<lastRtt->get()<<std::endl;
    }
    static const std::string content("Hello, world!");

    // Create Data packet
    auto data = make_shared<Data>(interest.getName());
    data->setFreshnessPeriod(10_s);
    data->setContent(reinterpret_cast<const uint8_t*>(content.data()), content.size());
    data->setTag(make_shared<lp::RetranTimesTag>(99999));
    data->setTag(make_shared<lp::FullRttTag>(123));
    data->setTag(make_shared<lp::LastRttTag>(456));
    std::cout<<"interest.getTag<lp::FullRttTag>()->get() is "<<interest.getTag<lp::FullRttTag>()->get()<<std::endl;

    auto start = time::steady_clock::now();
    for(int i = 0;  i < 99999; i++){
      for(int j = 0; j < 9999; j++){

      }
    }
    
    time::duration<double, time::nanoseconds::period> timeElapsed = time::steady_clock::now() - start;
    std::cout<<"time is "<<timeElapsed.count() / 1e9 <<std::endl;
    //data->wireEncode();
    std::cout<<"data->getTag<lp::LcdInsertTag>()->get() is "<<data->getTag<lp::RetranTimesTag>()->get()<<std::endl;

    // Sign Data packet with default identity
    m_keyChain.sign(*data);
    // m_keyChain.sign(*data, signingByIdentity(<identityName>));
    // m_keyChain.sign(*data, signingByKey(<keyName>));
    // m_keyChain.sign(*data, signingByCertificate(<certName>));
    // m_keyChain.sign(*data, signingWithSha256());

    // Return Data packet to the requester
    std::cout << "<< D: " << *data << std::endl;
    m_face.put(*data);
  }

  void
  onRegisterFailed(const Name& prefix, const std::string& reason)
  {
    std::cerr << "ERROR: Failed to register prefix '" << prefix
              << "' with the local forwarder (" << reason << ")" << std::endl;
    m_face.shutdown();
  }

private:
  Face m_face;
  KeyChain m_keyChain;
};

} // namespace examples
} // namespace ndn

int
main(int argc, char** argv)
{
  try {
    ndn::examples::Producer producer;
    producer.run();
    return 0;
  }
  catch (const std::exception& e) {
    std::cerr << "ERROR: " << e.what() << std::endl;
    return 1;
  }
}
